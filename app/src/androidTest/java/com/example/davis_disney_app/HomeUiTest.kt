package com.example.davis_disney_app

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.closeSoftKeyboard
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class HomeUiTest {

    @get:Rule
    val activityScenarioRule = ActivityScenarioRule(
        MainActivity::class.java
    )

    @Test
    fun launchFragmentAndVerifyUI() {
        onView(withId(R.id.search_button)).check(matches(withText("Search")))
        onView(withId(R.id.search_button)).check(matches(isClickable()))

        onView(withId(R.id.search_edit_text)).check(matches(isDisplayed()))
        onView(withId(R.id.search_edit_text)).check(matches(isEnabled()))

        onView(withId(R.id.comic_book_title)).check(matches(withText("")))
    }

    @Test
    fun testButtonClick() {
        onView(withId(R.id.search_edit_text))
            .perform(ViewActions.clearText())
            .perform(ViewActions.typeText("33"),
                closeSoftKeyboard())

        onView(withId(R.id.search_button)).perform(click())

        onView(withId(R.id.loading_widget)).check(matches(isDisplayed()))
    }
}