package com.example.davis_disney_app.feature.home.model

sealed class HomeUiState {
    data class Results(
        val comic: Comic
    ): HomeUiState()

    data class Error(
        val error: String
    ): HomeUiState()

    object Empty: HomeUiState()

    object Loading: HomeUiState()
}
