# Davis Disney Demo

# Libraries

Retrofit, Gson, Coroutines, LiveData, Glide

# Notes

Didn't focus too much on UI styling, just laid out the components separately.
Focused on architecture of each layer to make the app easily scalable. 
Some things were done in the interest of time like: constants and keys not being stored properly, error handling not very robust, no navigation, no input validation.

Added some very basic tests.

