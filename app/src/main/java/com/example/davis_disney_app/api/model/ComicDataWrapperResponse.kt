package com.example.davis_disney_app.api.model

import com.google.gson.annotations.SerializedName

data class ComicDataWrapperResponse(
    @SerializedName("code")
    val code: Int?,
    @SerializedName("data")
    val data: ComicDataContainerResponse?,
)