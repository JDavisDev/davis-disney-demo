package com.example.davis_disney_app.feature.home.model

data class Comic(
    val title: String,
    val description: String,
    val thumbnail: ComicImage,
)

data class ComicImage(
    val extension: String?,
    val path: String?,
)