package com.example.davis_disney_app.api.model

import com.google.gson.annotations.SerializedName

data class ComicDataContainerResponse(
    @SerializedName("results")
    val results: List<ComicResponse>?,
)
