package com.example.davis_disney_app

import androidx.test.platform.app.InstrumentationRegistry
import com.example.davis_disney_app.api.RetrofitService
import com.example.davis_disney_app.feature.home.ComicRepository
import com.example.davis_disney_app.feature.home.HomeViewModel
import com.example.davis_disney_app.feature.home.model.HomeUiState
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
//import org.mockito.Mockito.*

class HomeViewModelTest {
    private lateinit var viewModel: HomeViewModel
    var comicRepository: ComicRepository = ComicRepository(RetrofitService.comicApi)

    @Before
    fun setUp() {
        viewModel = HomeViewModel()

//        `when`(viewModel.onSearchClicked(anyString())).then { null } // can be modified to return unique states
    }

    @After
    fun tearDown() {
    }

    @Test
    fun testDefaultValues() {
        assertTrue(viewModel.uiState.value == null)
    }

    // suspend function so this test won't fully work, but adding for demonstration
    // need to add some coroutine rules
//    @Test
//    fun testSearchClicked() = runBlocking {
//        viewModel.onSearchClicked("42")
//        verify(comicRepository.fetchComicById("42"))
//        assertTrue(viewModel.uiState.value is HomeUiState.Error)
//    }
}