package com.example.davis_disney_app.api.model

import com.google.gson.annotations.SerializedName

data class ComicResponse(
    @SerializedName("title")
    val title: String?,
    @SerializedName("description")
    val description: String?,
    @SerializedName("thumbnail")
    val thumbnail: ImageResponse?,
)

data class ImageResponse(
    @SerializedName("path")
    val path: String?,
    @SerializedName("extension")
    val extension: String?,
)