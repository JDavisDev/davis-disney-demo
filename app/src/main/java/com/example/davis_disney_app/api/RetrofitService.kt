package com.example.davis_disney_app.api

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitService {
    private val apiInterceptor = Interceptor {
        val originalRequest = it.request()
        val newHttpUrl = originalRequest.url().newBuilder()
            .addQueryParameter("apikey", API_KEY)
            .build()
        val newRequest = originalRequest.newBuilder()
            .url(newHttpUrl)
            .build()
        it.proceed(newRequest)
    }

    private val client = OkHttpClient().newBuilder()
        .addNetworkInterceptor(apiInterceptor)
        .build()


    private fun getRetrofit() = Retrofit.Builder()
        .baseUrl("https://gateway.marvel.com/")
//        .client(client)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    val comicApi: ComicApi = getRetrofit().create(ComicApi::class.java)
}