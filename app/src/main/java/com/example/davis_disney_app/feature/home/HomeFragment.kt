package com.example.davis_disney_app.feature.home

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import com.bumptech.glide.Glide
import com.bumptech.glide.annotation.GlideModule
import com.example.davis_disney_app.R
import com.example.davis_disney_app.databinding.HomeFragmentBinding
import com.example.davis_disney_app.feature.home.model.HomeUiState

@GlideModule
class HomeFragment : Fragment() {

    companion object {
        fun newInstance() = HomeFragment()
    }

    private var _binding: HomeFragmentBinding? = null
    private val binding get() = _binding!!
    private val viewModel: HomeViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = HomeFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupComicListener()
        setupUiListener()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setupUiListener() {
        binding.searchButton.setOnClickListener {
            // validate input
            viewModel.onSearchClicked(
                binding.searchEditText.text.toString()
            )
        }
    }

    private fun setupComicListener() {
        viewModel.uiState.observe(viewLifecycleOwner) { comic ->
            when (comic) {
                is HomeUiState.Loading -> {
                    binding.loadingWidget.isVisible = true
                }
                is HomeUiState.Results -> {
                    binding.loadingWidget.isVisible = false
                    updateUiWithComic(comic)
                }
                is HomeUiState.Empty -> {
                    binding.loadingWidget.isVisible = false
                    binding.comicBookTitle.text = requireContext().getString(R.string.no_comic_found_message)
                }
                is HomeUiState.Error -> {
                    binding.loadingWidget.isVisible = false
                    binding.comicBookTitle.text = requireContext().getString(R.string.error_loading_comic_message)
                }
            }
        }
    }

    private fun updateUiWithComic(uiState: HomeUiState.Results) {
        print("Comic received! $uiState")
        val comic = uiState.comic
        val imageUrl = comic.thumbnail.path + "." + comic.thumbnail.extension

        binding.comicBookTitle.text = comic.title
        binding.comicBookDescription.text = comic.description

        Glide.with(this)
            .load(imageUrl)
            .fitCenter()
            .into(binding.comicImageView)
    }

}