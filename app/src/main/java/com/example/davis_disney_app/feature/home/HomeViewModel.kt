package com.example.davis_disney_app.feature.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.davis_disney_app.api.RetrofitService
import com.example.davis_disney_app.feature.home.model.Comic
import com.example.davis_disney_app.feature.home.model.HomeUiState
import kotlinx.coroutines.launch

class HomeViewModel : ViewModel() {

    // Future ideas: Load the comic before and after the current comic ID
    // When user searches, load the next and previous ID that has a valid comic
    // So when the user taps "Next" or "Previous" the comic is ready to go, and we can then load another.
    private var _uiState: MutableLiveData<HomeUiState> = MutableLiveData()
    val uiState: LiveData<HomeUiState> = _uiState

    private fun updateState(comicList: List<Comic>?) {
        when {
            comicList == null -> {
                _uiState.value = HomeUiState.Error(
                    "Error fetching comic for that ID"
                )
            }
            comicList.isEmpty() -> {
                _uiState.value = HomeUiState.Empty
            }
            comicList.isNotEmpty() -> {
                _uiState.value = HomeUiState.Results(
                    comicList[0]
                )
            }
        }
    }

    fun onSearchClicked(comicId: String) {
        _uiState.value = HomeUiState.Loading

        viewModelScope.launch {
            // the fetch is run on IO, but the returned value is back on the main thread
            // Not a great way to handle these dependencies, it's injection, but Dagger or Hilt would be the way to go
            // Just a little heavy for this project.
            val uiModel = ComicRepository(RetrofitService.comicApi).fetchComicById(comicId)
            updateState(uiModel)
        }
    }
}