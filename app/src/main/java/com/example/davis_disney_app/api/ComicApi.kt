package com.example.davis_disney_app.api

import com.example.davis_disney_app.api.model.ComicDataContainerResponse
import com.example.davis_disney_app.api.model.ComicDataWrapperResponse
import com.example.davis_disney_app.api.model.ComicResponse
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.http.Query

// This is not a good pattern. These keys should be held somewhere safe and secret.
// Just dropping them here for the demo.
const val API_KEY = "9484b9a23c0d629d20809e8453365294"
const val API_PRIVATE_KEY = "475a608c2aaf473a1b149b49799885e57830f303"
const val TS_KEY = "1"

interface ComicApi {
    @GET("v1/public/comics/{comicId}")
    suspend fun getComicById(
        @Path("comicId") comicId: String,
        @Query("ts") ts: String = "1",
        @Query("apikey") apikey: String = "9484b9a23c0d629d20809e8453365294",
        @Query("hash") hash: String
    ): Response<ComicDataWrapperResponse>
}