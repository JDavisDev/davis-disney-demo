package com.example.davis_disney_app.feature.home

import com.example.davis_disney_app.api.API_KEY
import com.example.davis_disney_app.api.API_PRIVATE_KEY
import com.example.davis_disney_app.api.ComicApi
import com.example.davis_disney_app.api.TS_KEY
import com.example.davis_disney_app.api.model.ComicResponse
import com.example.davis_disney_app.feature.Util
import com.example.davis_disney_app.feature.home.model.Comic
import com.example.davis_disney_app.feature.home.model.ComicImage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

/**
 * This class is responsible for deciding how to fetch the data - could be an api or database
 * Responsible for returning the objects necessary, and the VM can handle the actual UI State
 */
class ComicRepository(private val api: ComicApi) {

    suspend fun fetchComicById(comicId: String): List<Comic>? {
        val response = withContext(Dispatchers.IO) {
            api.getComicById(
                comicId = comicId,
                // somewhat hacky way of computing this, prob should be moved to a separate network util layer or an interceptor
                hash = Util.md5(TS_KEY + API_PRIVATE_KEY + API_KEY)
            )
        }

        // given more time: this should be wrapped in an object so the VM can properly relay state
        // could handle an actual error message
        return if (response.isSuccessful) {
            toListOfComics(response.body()?.data?.results.orEmpty())
        } else {
            null
        }

    }

    private fun toListOfComics(comicResponseList: List<ComicResponse>): List<Comic> {
        return comicResponseList.map {
            // this logic would depend on business reqs. Do we toss out the entire comic if a field is null?
            Comic(
                title = it.title ?: "No title",
                description = it.description ?: "No description",
                thumbnail = ComicImage(it.thumbnail?.extension, it.thumbnail?.path)
            )
        }.toList()
    }
}