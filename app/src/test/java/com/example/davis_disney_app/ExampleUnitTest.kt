package com.example.davis_disney_app

import com.example.davis_disney_app.api.API_KEY
import com.example.davis_disney_app.api.API_PRIVATE_KEY
import com.example.davis_disney_app.api.TS_KEY
import com.example.davis_disney_app.feature.Util
import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    // Pretty simple test, but this will validate the hash is the expected value
    // and will guard against this being changed
    @Test
    fun testMd5Hash() {
        val actualHash = Util.md5(TS_KEY + API_PRIVATE_KEY + API_KEY)
        val expected = "ade9a50f8d0a334e208681bee6e98115"
        assertEquals(actualHash, expected)
    }
}